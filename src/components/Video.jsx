


import YouTube from 'react-youtube';

function Video() {
  const onPlayerReady = (event) => {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }

  const opts  = {
    height: '390',
    width: '640',
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 1,
    },
  };

  return <YouTube videoId="i51olb4HBgU" opts={opts} onReady={onPlayerReady} />;
}

export default Video
